Tepl - Text editor product line
===============================

This is version 6.00.0 of Tepl.

Tepl is a library that eases the development of GtkSourceView-based text
editors and IDEs.

Tepl was previously named Gtef (GTK text editor framework). The project has
been renamed in June 2017 to have a more beautiful name. The end of Tepl is
pronounced like in “apple”.

The Tepl library is free software and is released under the terms of the GNU
Lesser General Public License, see the `LICENSES/` directory for more
information.

See also the [Tepl web page](https://wiki.gnome.org/Projects/Tepl).

Dependencies
------------

- GLib >= 2.64
- GTK >= 3.22
- GtkSourceView >= 4.0
- [Amtk](https://wiki.gnome.org/Projects/Amtk) >= 5.0
- [ICU](http://site.icu-project.org/)

Installation
------------

To build the latest version of Tepl plus its dependencies from Git,
[Jhbuild](https://wiki.gnome.org/Projects/Jhbuild) is recommended.

Documentation
-------------

See the `gtk_doc` Meson option. A convenient way to read the API documentation
is with the [Devhelp](https://wiki.gnome.org/Apps/Devhelp) application.

Contributions
-------------

Contributions are no longer accepted for this project. See the `HACKING` file
for more information.
